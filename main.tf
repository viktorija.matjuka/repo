provider "aws" {
  profile = "default"
  region  = "eu-central-1"
  #shared_credentials_file = "C:/Users/viktorija.matjuka/credentials"
}

#
#-we are trying to find latest 04 Ubuntu image 
#
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "learning_instance" {
  ami           = data.aws_ami.ubuntu.image_id
  key_name      = aws_key_pair.vika.key_name
  instance_type = "t2.micro"
    tags = {
    Name = "learning_instance"
  }

resource "aws_key_pair" "vika" {
  key_name   = "vika-key"
  public_key = file(pathexpand("~/.ssh/id_rsa.pub"))
}

#S3 Bycket
resource "aws_s3_bucket" "buranbucket" {
  bucket = "buranbucket"
  acl = "private"
   tags = {
    Name = "Buran_bucket"
    Environment = "Dev"
  }

#IAM Role
resource "aws_iam_role" "learning_role" {
  name = "learning role"

  assume_role_policy = file("iamrole.json")
}
#IAM Role policy
resource "aws_iam_role_policy" "learning_policy" {
  name = "learning_policy"
  role = aws_iam_role.learning_policy.id

  policy = file("iamrolepolicy.json")
}
